package core.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import core.beans.CouponBean;
import core.enums.CouponType;
/**
 * JBean representing a Coupon.
 * Coupons have an <b>ID</b>, <b>title</b>, <b>start date</b>, <b>end date</b>, <b>amount</b>, <b>coupon type</b>, <b>message</b>, <b>price</b>, and <b>a path to a coupon-related image</b>.
 */
@Entity
@Table(name = "coupon")
public class CouponEntity implements Serializable{

	private static final long serialVersionUID = -927238060353756890L;
	@Id
	@GeneratedValue
	@Column(name = "couponId", nullable = false)
	private long couponId;
	@Column(name = "title", nullable = false)
	private String title;
	@Column(name = "startDate", nullable = false)
	private Date startDate;
	@Column(name = "endDate", nullable = false)
	private Date endDate;
	@Column(name = "amount", nullable = false)
	private int amount;
	@Column(name = "type", nullable = false)
	private CouponType type;
	@Column(name = "message", nullable = false)
	private String message;
	@Column(name = "price", nullable = false)
	private double price;
	@Column(name = "image", nullable = false)
	private String image;
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private CompanyEntity company;
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Collection<CustomerEntity> customers;

	/**
	 * Empty Coupon bean constructor.
	 */
	public CouponEntity() {
		
	}

	
	public CouponEntity(CouponBean coupon) {
		super();
		this.couponId = coupon.getCouponId();
		this.title = coupon.getTitle();
		this.startDate = coupon.getStartDate();
		this.endDate = coupon.getEndDate();
		this.amount = coupon.getAmount();
		this.type = coupon.getType();
		this.message = coupon.getMessage();
		this.price = coupon.getPrice();
		this.image = coupon.getImage();
		CompanyEntity company = new CompanyEntity();
		company.setId(coupon.getCompanyId());
		this.company = company;
//		this.customers = customers;
	}


	/**
	 * Gets the coupon's ID number.
	 * @return The coupon's ID number.
	 */
	public long getCouponId() {
		return couponId;
	}

	/**
	 * Sets the coupon's ID number.
	 * @param id The coupon's ID number.
	 */
	public void setCouponId(long id) {
		this.couponId = id;
	}

	/**
	 * Gets the coupon's title.
	 * @return The coupon's title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the coupon's title.
	 * @param title The coupon's title.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the coupon's start date.
	 * @return The coupon's start date.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the coupon's start date.
	 * @param startDate The coupon's start date.
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the coupon's end date.
	 * @return The coupon's end date.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the coupon's end date.
	 * @param endDate The coupon's end date.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the available amount of this coupon to sell.
	 * @return The available amount of this coupon to sell.
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * Sets the available amount of this coupon to sell.
	 * @param amount The available amount of this coupon to sell.
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	/**
	 * Gets the coupon's CouponType.
	 * @return The coupon's CouponType.
	 */
	public CouponType getType() {
		return type;
	}

	/**
	 * Sets the coupon's CouponType.
	 * @param type The coupon's CouponType.
	 */
	public void setType(CouponType type) {
		this.type = type;
	}

	/**
	 * Gets the coupon's message.
	 * @return The coupon's message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the coupon's message.
	 * @param message The coupon's message.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the coupon's price.
	 * @return The coupon's price.
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Sets the coupon's price.
	 * @param price The coupon's price.
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Gets the path to the image related to the coupon.
	 * @return The path to the image related to the coupon.
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Sets the path to the image related to the coupon.
	 * @param image The path to the image related to the coupon.
	 */
	public void setImage(String image) {
		this.image = image;
	}

	
	public CompanyEntity getCompany() {
		return company;
	}

	public void setCompany(CompanyEntity company) {
		this.company = company;
	}

	public Collection<CustomerEntity> getCustomers() {
		return customers;
	}

	public void setCustomers(Collection<CustomerEntity> customers) {
		this.customers = customers;
	}

	/**
	 * String representation of the Coupon's data.
	 * @return A String representation of the Coupon's data.
	 */
	@Override
	public String toString() {
		return "Coupon [id=" + couponId + ", title=" + title + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", amount=" + amount + ", type=" + type + ", message=" + message + ", price=" + price + "]";
	}

	
	/*
	 * (non-Javadoc)
	 * Compare 2 Coupon beans
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CouponEntity other = (CouponEntity) obj;
		/*if (amount != other.amount)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;*/
		if (couponId != other.couponId)
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	public CouponBean toBean() {
		CouponBean coupon = new CouponBean();
		coupon.setCouponId(this.couponId);
		coupon.setTitle(this.title);
		coupon.setStartDate(this.startDate);
		coupon.setEndDate(this.endDate);
		coupon.setAmount(this.amount);
		coupon.setType(this.type);
		coupon.setMessage(this.message);
		coupon.setPrice(this.price);
		coupon.setImage(this.image);
		coupon.setCompanyId(this.company.getId());
		return coupon;
	}
	
	public void addCustomer(CustomerEntity customer) {
		customers.add(customer);
        customer.getCoupons().add(this);
    }
 
    public void removeCustomer(CustomerEntity customer) {
    	customers.remove(customer);
        customer.getCoupons().remove(this);
    }
 
    public void remove() {
        for(CustomerEntity customer : new ArrayList<>(customers)) {
        	removeCustomer(customer);
        }
    }
}
