package core.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import core.beans.CustomerBean;

/**
 * JBean representing a Customer.
 * Customers have an <b>ID</b>, <b>customer name</b>, <b>password</b>, and a <b>Collection of Coupons they bought</b>.
 */

@Entity
@Table(name = "customer")
public class CustomerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/* Attributes */
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private long id;
	@Column(name = "custName", nullable = false)
	private String custName;
	@Column(name = "password", nullable = false)
	private String password;
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Collection <CouponEntity> coupons;
	
	/* Constructors */

	/**
	 * Empty Customer bean constructor.
	 */
	public CustomerEntity( ) {
	}
	
	/* Public Methods */

	public CustomerEntity(CustomerBean customer) {
		// TODO Auto-generated constructor stub
		this.id = customer.getId();
		this.custName = customer.getCustName();
		this.password = customer.getPassword();		
	}

	/**
	 * Gets the customer's ID number.
	 * @return The customer's ID number.
	 */
	public long getId( ) {
		return id;
	}

	/**
	 * Sets the customer's ID number.
	 * @param id The customer's ID number.
	 */
	public void setId( long id ) {
		this.id = id;
	}

	/**
	 * Gets the customer's name.
	 * @return The customer's name.
	 */
	public String getCustName( ) {
		return custName;
	}

	/**
	 * Sets the customer's name.
	 * @param custName The customer's name.
	 */
	public void setCustName( String custName ) {
		this.custName = custName;
	}

	/**
	 * Gets the customer's password.
	 * @return The customer's password.
	 */
	public String getPassword( ) {
		return password;
	}

	/**
	 * Sets the customer's password.
	 * @param password The customer's password.
	 */
	public void setPassword( String password ) {
		this.password = password;
	}

	/**
	 * Gets the customer's Coupons Collection.
	 * @return The customer's Coupons Collection.
	 */
	public Collection<CouponEntity> getCoupons( ) {
		return coupons;
	}

	/**
	 * Sets the customer's Coupons Collection.
	 * @param coupons The customer's Coupons Collection.
	 */
	public void setCoupons(Collection<CouponEntity> coupons) {
		this.coupons = coupons;
	}
	

	/**
	 * String representation of the Customer's data.
	 * @return A String representation of the Customer's data.
	 */
	@Override
	public String toString() {
		return "Customer [id=" + id + ", custName=" + custName + ", password=" + password + ", coupons=" + coupons
				+ "]";
	}

	

	/*
	 * (non-Javadoc)
	 * Compare 2 Customer beans
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerEntity other = (CustomerEntity) obj;
		/*if (coupons == null) {
			if (other.coupons != null)
				return false;
		} else if (!coupons.equals(other.coupons))
			return false;*/
		if (custName == null) {
			if (other.custName != null)
				return false;
		} else if (!custName.equals(other.custName))
			return false;
		if (id != other.id)
			return false;
		/*if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;*/
		return true;
	}

	public CustomerBean toBean() {
		CustomerBean bean = new CustomerBean();
		bean.setId(this.id);
		bean.setCustName(this.custName);
		bean.setPassword("*PASSWORD*");
		// TODO Auto-generated method stub
		return bean;
	}
	
	public void addCoupon(CouponEntity coupon) {
		coupons.add(coupon);
        coupon.getCustomers().add(this);
    }
 
    public void removeCoupon(CouponEntity coupon) {
    	coupons.remove(coupon);
        coupon.getCustomers().remove(this);
    }
 
    public void remove() {
        for(CouponEntity coupon : new ArrayList<>(coupons)) {
        	removeCoupon(coupon);
        }
    }
}