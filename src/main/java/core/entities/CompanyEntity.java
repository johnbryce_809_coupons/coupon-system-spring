package core.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import core.beans.CompanyBean;


/**
 * JBean representing a Company.
 * Companies have an <b>ID</b>, <b>company name</b>, <b>password</b>, <b>e-mail</b> and a <b>Collection of Coupons they sell</b>.
 */
@Entity
@Table(name = "company") 
public class CompanyEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( )
	@Column(name = "id", nullable = false)
	private long id;
	
	@Column(name = "compName", nullable = false)
	private String compName;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "email", nullable = false)
	private String email;
	
	@OneToMany(mappedBy = "company", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY )
	private Collection<CouponEntity> coupons;


	/**
	 * Empty Company bean constructor.
	 */
	public CompanyEntity() {

	}

	/**
	 * Gets the company's ID number.
	 * @return The company's ID number.
	 */
	public long getId() {
		return id;
	}

	
	public CompanyEntity(long id) {
		this.id = id;
	}

	public CompanyEntity(CompanyBean company) {
		this.id = company.getId();
		this.compName = company.getCompName();
		this.password = company.getPassword();
		this.email = company.getEmail();
	}

	/**
	 * Sets the company's ID number.
	 * @param id The company's ID number.
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the company's name.
	 * @return The company's name.
	 */
	public String getCompName() {
		return compName;
	}

	/**
	 * Sets the company's name.
	 * @param compName The company's name.
	 */
	public void setCompName(String compName) {
		this.compName = compName;
	}

	/**
	 * Gets the company's password.
	 * @return The company's password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the company's password.
	 * @param password The company's password.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the company's e-mail.
	 * @return email The company's e-mail.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the company's e-mail.
	 * @param email The company's e-mail.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the company's Coupons Collection.
	 * @return The company's Coupons Collection.
	 */
	public Collection<CouponEntity> getCoupons() {
		return coupons;
	}

	/**
	 * Sets the company's Coupons Collection.
	 * @param coupons The company's Coupons Collection.
	 */
	public void setCoupons(Collection<CouponEntity> coupons) {
		this.coupons = coupons;
	}


	/**
	 * String representation of the Company's data.
	 * @return A String representation of the Company's data.
	 */
	@Override
	public String toString() {
		return "Company [id=" + id + ", compName=" + compName + ", password=" + password + ", email=" + email
				+ ", coupons=" + coupons + "]";
	}

	/*
	 * (non-Javadoc)
	 * Compare 2 Company beans
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompanyEntity other = (CompanyEntity) obj;
		if (compName == null) {
			if (other.compName != null)
				return false;
		} else if (!compName.equals(other.compName))
			return false;
		/*if (coupons == null) {
			if (other.coupons != null)
				return false;
		} else if (!coupons.equals(other.coupons))
			return false;*/
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		/*if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;*/
		return true;
	}
	
	public CompanyBean toBean() {
		CompanyBean bean = new CompanyBean();
		bean.setId(this.id);
		bean.setCompName(this.compName);
		bean.setPassword("*PASSWORD*");
		bean.setEmail(this.email);
		// TODO Auto-generated method stub
		return bean;
	}

	public void addCoupon(CouponEntity coupon) {
		coupons.add(coupon);
        coupon.setCompany(this);
    }
 
    public void removeCoupon(CouponEntity coupon) {
    	coupons.remove(coupon);
        coupon.remove();
    }
 
    public void remove() {
        for(CouponEntity coupon : new ArrayList<>(coupons)) {
        	removeCoupon(coupon);
        }
    }
}
