package core.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import core.beans.CustomerBean;
import core.entities.CustomerEntity;
import core.enums.UserType;
import core.exception.CouponSystemException;
import core.exception.ExceptionsEnum;
import core.repository.ICustomerRepository;

/**
 * Facade used to access the coupon system by Customers
 *
 */
@Service
public class CustomerService implements Serializable, IBeanValidatorConstants{
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private ICustomerRepository customerDAO;

	/**
	 * Adds a new customer entity to the repository.
	 * 
	 * @param customer the new customer entity to be added.
	 * @return the created customer's ID. 
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as :
	 * 	existing name, (3) Invalid data.
	 */
	public long createCustomer(CustomerBean customer) throws CouponSystemException {
		//can be used if customer registration should be restricted 
		
		checkCustomer(customer);
		//CLD BE HANDLED BY DAO LAYER BY MAKING IT UNIQUE
		if(customerDAO.existsByCustName(customer.getCustName())) {
			throw new CouponSystemException(ExceptionsEnum.NAME_EXISTS,"Customer Name already exists");
		}

		long customerId = customerDAO.save(new CustomerEntity(customer)).getId();
		System.out.println("LOG : Customer created : " + customer);
		return customerId;
	}

	/**
	 * Updates a customer entity in the repository.
	 * 
	 * @param customer the customer object to be updated.
	 * @param userId the user updating the customer
	 * @param userType the user type updating the customer
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 * 	(3) Invalid data, (4) security breach.
	 */
	@Transactional
	public void updateCustomer(CustomerBean customer, long userId, UserType userType) throws CouponSystemException {		
		if ((customer.getId() != userId || !userType.equals(UserType.CUSTOMER)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to update customer %s",userType , userId, customer));
		}
		
		CustomerEntity originalCustomer = this.getCustomer(customer.getId());
		originalCustomer.setCustName(customer.getCustName());
		checkCustomer(originalCustomer.toBean());
		if(customerDAO.updateCustomer(originalCustomer.getCustName(), originalCustomer.getId())==0) {
			//SHLD NEVER HAPPEN - CLIENT SIDE ERROR
			throw new CouponSystemException(ExceptionsEnum.FAILED_OPERATION,"update customer failed, ID : " + customer.getId());
		}
		System.out.println(String.format("LOG : User %s %s updated customer %s",userType , userId, customer));		
	}

	/**
	 * updates the customer's password
	 * 
	 * @param customerId The customer to update
	 * @param newPassword The new password
	 * @param oldPassword The old password
	 * @param userId the user updating the coupon
	 * @param userType the user type updating the coupon
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts.
	 */
	public void updateCustomerPassword(long customerId, String oldPassword, String newPassword, long userId, UserType userType) throws CouponSystemException {
		if ((customerId != userId || !userType.equals(UserType.CUSTOMER)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to change customer's %s password ",userType , userId, customerId));
		}
		if(userType.equals(UserType.CUSTOMER)) {
			this.customerLogin(customerId, oldPassword);
		}
		checkCustomerPassword(newPassword);
		if(customerDAO.updateCustomerPassword(customerId, newPassword)==0) {
			//SHLD NEVER HAPPEN - CLIENT SIDE ERROR
			throw new CouponSystemException(ExceptionsEnum.FAILED_OPERATION,"update customer's password failed, ID : " + customerId);
		}
		System.out.println(String.format("LOG : User %s %s changed customer's %s password ",userType , userId, customerId));
	}
	
	/**
	 * Removes a customer entity from the customers repositories.
	 * removes the customer's coupons as well.
	 * 
	 * @param customerId the customer's ID.
	 * @param userId the user removing the customer.
	 * @param userType the user type
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 *  (3) Invalid data, (4) security breach.
	 */
	@Transactional
	public void removeCustomer(long customerId, long userId, UserType userType) throws CouponSystemException {
		//can be modified if customer removing should be restricted
		if ((customerId != userId || !userType.equals(UserType.CUSTOMER)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to remove customer %s",userType , userId, customerId));
		}
		
		CustomerEntity originalCustomer = this.getCustomer(customerId);	
		originalCustomer.remove();
		customerDAO.save(originalCustomer);//TODO is necessary ?
		customerDAO.deleteById(customerId);	
			
		System.out.println(String.format("LOG : User %s %s removed customer %s",userType , userId, customerId));			
	}

	/**
	 * Retrieves a customer entity from the repository.
	 * 
	 * @param customerId the customer's ID.
	 * @return a CustomerBean object
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	public CustomerEntity getCustomer(long customerId) throws CouponSystemException {
		Optional<CustomerEntity> optionalCustomer = customerDAO.findById(customerId);
		if(!optionalCustomer.isPresent()) {
			throw new CouponSystemException(ExceptionsEnum.FAILED_OPERATION,"can't find customer with id num : " + customerId);
		}
		return optionalCustomer.get();
	}

	/**
	 * Retrieves all the customers entities from the repository .
	 * 
	 * @return a Collection of customers objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	public Collection<CustomerBean> getAllCustomers() throws CouponSystemException{
		Collection<CustomerBean> beans = new ArrayList<>();
		for (CustomerEntity customerEntity : customerDAO.findAll()) {
			beans.add(customerEntity.toBean());
		}		
		return beans;
	}

	/**
	 * Check the password and user name,
	 * returns the user ID if correct.
	 * 
	 * @param customerName The customer's user name
	 * @param password The customer's password
	 * @return a long userId - the user's ID
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 * 
	 */
	public long customerLogin(String customerName, String password) throws  CouponSystemException {
		Optional<Long> optionalId = customerDAO.customerLogin(customerName, password);
		if(!optionalId.isPresent()) {
			throw new CouponSystemException(ExceptionsEnum.AUTHENTICATION,"Incorrect Name Or Password");
		}
		return optionalId.get();
	}

	public long customerLogin(long customerId, String password) throws  CouponSystemException {
		Optional<Long> optionalId = customerDAO.customerLogin(customerId, password);
		if(!optionalId.isPresent()) {
			throw new CouponSystemException(ExceptionsEnum.AUTHENTICATION,"Incorrect ID Or Password");
		}
		return optionalId.get();
	}
	
	private void checkCustomer(CustomerBean customer) throws CouponSystemException {
		checkCustomerName(customer.getCustName());
		checkCustomerPassword(customer.getPassword());
	}

	private void checkCustomerName(String custName) throws CouponSystemException {
		if (custName.length() > CUSTOMER_NAME_LENGTH) {
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_NAME,"The customer password cant be longer than " + PASSWORD_MAX_LENGTH + " characters");
		}
	}

	private void checkCustomerPassword(String password) throws CouponSystemException {
		if (password.length() > PASSWORD_MAX_LENGTH) {
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_PASSWORD_MAX,"The customer password can't be be longer than " + PASSWORD_MAX_LENGTH + " characters");
		}
		if (password.length() < PASSWORD_MIN_LENGTH) {
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_PASSWORD_MIN,"The customer password can't be shorter than " + PASSWORD_MIN_LENGTH + " characters");
		}
	}
}
