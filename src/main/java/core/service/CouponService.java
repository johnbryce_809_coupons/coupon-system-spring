/**
 * 
 */
package core.service;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import core.beans.CartBean;
import core.beans.CouponBean;
import core.entities.CompanyEntity;
import core.entities.CouponEntity;
import core.entities.CustomerEntity;
import core.enums.CartStatus;
import core.enums.CouponType;
import core.enums.UserType;
import core.exception.CouponSystemException;
import core.exception.ExceptionsEnum;
import core.repository.ICouponRepository;

/**
 * Service used to access coupon related operations
 */
@Service
public class CouponService implements Serializable, IBeanValidatorConstants{
	private static final long serialVersionUID = 1L;

	@Autowired
	private ICouponRepository couponDAO;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CustomerService customerService;

	/**
	 * Adds a new coupon entity to the repository.
	 * 
	 * @param coupon the new CouponBean object to be added.
	 * @param userId the user creating the new coupon
	 * @return the created coupon's ID. 
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as :
	 * 	existing title, (3) Invalid data, (4) security breach.
	 */
	@Transactional
	public long createCoupon(CouponBean coupon, long userId, UserType userType) throws CouponSystemException {
		checkCoupon(coupon);
		//check if userId matches coupon's companyId
		if ((coupon.getCompanyId() != userId || !userType.equals(UserType.COMPANY)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to create the coupon %s", userType, userId, coupon));
		}
		//CLD BE HANDLED BY DAO LAYER BY MAKING IT UNIQUE
		if (couponDAO.existsByTitle(coupon.getTitle())) {
			throw new CouponSystemException(ExceptionsEnum.NAME_EXISTS,"Coupon Title already exists");
		}
		
		CompanyEntity companyEntity = companyService.getCompany(coupon.getCompanyId());
		CouponEntity couponEntity = new CouponEntity(coupon);
		couponEntity.setCompany(companyEntity);	
		couponEntity = couponDAO.save(couponEntity);
		System.out.println(String.format("LOG : %s %s created coupon %s", userType, userId, couponEntity.getCouponId()));
		return couponEntity.getCouponId();
	}


	/**
	 * Adds a coupon to a customer entity, and updates the entity's amount in the repository.
	 * cannot be resolve if it results in a negative coupon's amount, or if customer already owns this coupon. 
	 * 
	 * @param couponId the coupon's ID.
	 * @param userId the user ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : out of stock,
	 *  existing ownership or no matching data.
	 */
	@Transactional(isolation=Isolation.SERIALIZABLE)
	public void purchaseCoupon(long couponId, long customerId, long userId, UserType userType) throws CouponSystemException {
		//checks userType
		if( (!userType.equals(UserType.CUSTOMER) || customerId != userId) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to purchase coupon %s on user %s", userType, userId, couponId, customerId));
		}
				
		CouponEntity coupon = this.getCoupon(couponId);
		CustomerEntity customer = customerService.getCustomer(customerId);
		if(coupon.getCustomers().contains(customer)) {
			throw new CouponSystemException(ExceptionsEnum.CUSTOMER_OWNS_COUPON,"Customer already owns coupon");
		}		
		coupon.addCustomer(customer);
//		if(couponDAO.updateCouponAmount(couponId, -1)==0) {
//			throw new CouponSystemException(ExceptionsEnum.DATA_BASE_ACCSESS,"purchase coupon failed : " + couponId);
//		}
		coupon.setAmount(coupon.getAmount()-1);
		couponDAO.save(coupon);
		
		System.out.println(String.format("User %s %s purchased coupon %s", userType, userId, couponId));
	}


	
	/**
	 * Adds a coupon to a customer entity, and updates the entity's amount in the repository.
	 * cannot be resolve if it results in a negative coupon's amount, or if customer already owns this coupon. 
	 * 
	 * @param couponId the coupon's ID.
	 * @param userId the user ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : out of stock,
	 *  existing ownership or no matching data.
	 */
	@Transactional(isolation=Isolation.SERIALIZABLE)
	public void cancelPurchaseCoupon(long couponId, long customerId, long userId, UserType userType) throws CouponSystemException {
		//checks userType
		if( (!userType.equals(UserType.CUSTOMER) || customerId != userId) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to cancel purchase coupon %s on user %s", userType, userId, couponId, customerId));
		}
		
		CouponEntity coupon = this.getCoupon(couponId);
		CustomerEntity customer = customerService.getCustomer(customerId);
		if(!coupon.getCustomers().contains(customer)) {
			throw new CouponSystemException(ExceptionsEnum.CUSTOMER_OWNS_COUPON,"Customer does not own coupon");
		}		
		coupon.removeCustomer(customer);
//		if(couponDAO.updateCouponAmount(couponId, 1)==0) {
//			throw new CouponSystemException(ExceptionsEnum.CANCEL_PURCHASE_CUSTOMER_FAILED,"cancel purchase coupon failed : " + couponId);
//		}
		coupon.setAmount(coupon.getAmount()+1);
		couponDAO.save(coupon);	
		
		System.out.println(String.format("User %s %s purchased coupon %s", userType, userId, couponId));
	}



	/**
	 * Updates a coupon entity in the repository.
	 * 
	 * @param coupon the CouponBean object to be updated.
	 * @param userId the user updating the coupon
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 * 	(3) Invalid data, (4) security breach.
	 */
	@Transactional
	public void updateCoupon(CouponBean coupon, long userId, UserType userType) throws CouponSystemException {
		// gets original coupon data
		CouponEntity originalCoupon = this.getCoupon(coupon.getCouponId());

		if (coupon.getCompanyId() != originalCoupon.getCompany().getId() && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to change ownership of coupon ", userType, userId, coupon));
		}
		if ((originalCoupon.getCompany().getId() != userId || !userType.equals(UserType.COMPANY)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to a coupon %s it doesn't own", userType, userId, coupon));
		}
		// alter the coupon data to the new ALLOWED ones
		originalCoupon.setEndDate(coupon.getEndDate());	
		originalCoupon.setPrice(coupon.getPrice());

		CompanyEntity company = companyService.getCompany(coupon.getCompanyId());
		originalCoupon.setCompany(company);
		originalCoupon.setImage(coupon.getImage());
		originalCoupon.setMessage(coupon.getMessage());
//		originalCoupon.setTitle(coupon.getTitle());
		originalCoupon.setType(coupon.getType());
		// updates the coupon
		checkCoupon(originalCoupon.toBean());
		couponDAO.save(originalCoupon);
		System.out.println(String.format("LOG : User %s %s updated coupon %s", userType, userId, coupon.getCouponId()));
	}
	
	/**
	 * Updates a coupon entity's amount in the repository.
	 * cannot be resolve if it results in a negative amount. 
	 * 
	 * @param couponId the coupon's ID.
	 * @param userId the user updating the coupon's amount.
	 * @param userType the user type
	 * @param amountDelta the amount of coupons to be added or removed (negative amount).
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : negative delta to exceeds stock,
	 *  no matching data, (3) Invalid data, (4) security breach.
	 */
	public void updateCouponAmout(long couponId, int amountDelta, long userId, UserType userType) throws CouponSystemException {
		// gets original coupon data
		CouponEntity originalCoupon = this.getCoupon(couponId);
		
		if ((originalCoupon.getCompany().getId() != userId || !userType.equals(UserType.COMPANY)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to update a coupon's amount it doesn't own %s", userType, userId, couponId));
		}
		if (originalCoupon.getAmount()+amountDelta<0) {
			throw new CouponSystemException(ExceptionsEnum.AMOUNT_NOT_UPDATED,"negative amount is not allowed");
		}	
		if(couponDAO.updateCouponAmount(couponId, amountDelta)==0) {
			//NEGATIVE DELTA TO BIG or CLIENT SIDE ERROR
			throw new CouponSystemException(ExceptionsEnum.AMOUNT_NOT_UPDATED ,"update amount failed : " + couponId);
		}
		System.out.println(String.format("User %s %s updated coupon's amount %s by %s units", userType, userId, couponId, amountDelta));
	}

	/**
	 * Removes a coupon entity from the coupons and customers' coupons repositories.
	 * 
	 * @param couponId the coupon's ID.
	 * @param userId the user removing the coupon.
	 * @param userType the user type
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 *  (3) Invalid data, (4) security breach.
	 */
	@Transactional
	public void removeCoupon(long couponId, long userId, UserType userType) throws CouponSystemException {

		CouponEntity originalCoupon = this.getCoupon(couponId);
		if ((originalCoupon.getCompany().getId() != userId || !userType.equals(UserType.COMPANY)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to remove a coupon it doesn't own %s", userType, userId, couponId));
		}

		originalCoupon.remove();
		couponDAO.save(originalCoupon);
		couponDAO.deleteById(couponId);

		System.out.println(String.format("LOG : User %s %s removed coupon %s", userType, userId, couponId));
	}

	/**
	 * Retrieves a coupon entity from the repository.
	 * 
	 * @param couponId the coupon's ID.
	 * @return a CouponBean object
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	public CouponEntity getCoupon(long couponID) throws CouponSystemException {
		Optional<CouponEntity> optionalCoupon = couponDAO.findById(couponID);
		if(!optionalCoupon.isPresent()) {
			throw new CouponSystemException(ExceptionsEnum.FAILED_OPERATION,"can't find coupon with id num : " + couponID);
		}
		return optionalCoupon.get();
	}

	/**
	 * Retrieves all the coupons entities of said type from the repository .
	 * 
	 * @param type the coupons Type.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	public Collection<CouponBean> getCouponsByType(CouponType couponType) throws CouponSystemException {
		
		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.findByType(couponType)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}

	/**
	 * Retrieves all the coupons entities from the repository .
	 * 
	 * @return a Collection of coupons objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	public Collection<CouponBean> getAllCoupons() throws CouponSystemException {
		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.findAll()) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}

	/**
	 * Retrieves all the coupons entities for said Company from the repository .
	 * 
	 * @param companyId the company's Id.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@Transactional
	public Collection<CouponBean> getCompanyCoupons(long companyId) throws CouponSystemException {

		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.getCouponsByCompany_id(companyId)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}

	/**
	 * Retrieves all the coupons entities of said Type for said Company from the repository .
	 * 
	 * @param companyId the company's Id.
	 * @param type the coupons Type.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@Transactional
	public Collection<CouponBean> getCompanyCouponsByType(long companyId, CouponType type) throws CouponSystemException {

		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.getCouponsByCompany_idAndType(companyId, type)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}
	
	/**
	 * Retrieves all the coupons entities bellow said Price for said Company from the repository .
	 * 
	 * @param companyId the company's Id.
	 * @param price the coupons Price.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@Transactional
	public Collection<CouponBean> getCompanyCouponsByPrice(long companyId, double price) throws CouponSystemException {

		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.getCouponsByCompany_idAndPrice(companyId, price)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}
	
	/**
	 * Retrieves all the coupons entities expiring before said Date for said Company from the repository .
	 * 
	 * @param companyId the company's Id.
	 * @param date the latest (max) expiration Date.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@Transactional
	public Collection<CouponBean> getCompanyCouponsByDate(long companyId, Date date) throws CouponSystemException {

		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.getCouponsByCompany_idAndEndDate(companyId, date)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}
		
	/**
	 * Retrieves all the coupons entities for said Customer from the repository .
	 * 
	 * @param customerId the customer's Id.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@Transactional
	public Collection<CouponBean> getCustomerCoupons(long customerId, long userId, UserType userType) throws CouponSystemException {
		if( (!userType.equals(UserType.CUSTOMER) || customerId != userId) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to view user %s coupons", userType, userId, customerId));
		}

		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.getCouponsByCustomers_id(customerId)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}

	/**
	 * Retrieves all the coupons entities of said Type for said Customer from the repository .
	 * 
	 * @param customerId the customer's Id.
	 * @param type the coupons Type.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */	
	@Transactional
	public Collection<CouponBean> getCustomerCouponsByType(long customerId, CouponType type, long userId, UserType userType) throws CouponSystemException {
		if( (!userType.equals(UserType.CUSTOMER) || customerId != userId) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to view user %s coupons", userType, userId, customerId));
		}

		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.getCouponsByCustomers_idAndType(customerId, type)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}

	/**
	 * Retrieves all the coupons entities bellow said Price for said Customer from the repository .
	 * 
	 * @param customerId the customer's Id.
	 * @param price the coupons Price.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@Transactional
	public Collection<CouponBean> getCustomerCouponsByPrice(long customerId, double price, long userId, UserType userType) throws CouponSystemException {
		if( (!userType.equals(UserType.CUSTOMER) || customerId != userId) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to view user %s coupons", userType, userId, customerId));
		}

		Collection<CouponBean> beans = new ArrayList<>();
		for (CouponEntity couponEntity : couponDAO.getCouponsByCustomers_idAndPrice(customerId, price)) {
			beans.add(couponEntity.toBean());
		}		
		return beans;
	}


	private void checkCoupon(CouponBean coupon) throws CouponSystemException {
		checkTitle(coupon.getTitle());
//		checkStartDate(coupon.getStartDate());
		checkEndDate(coupon.getEndDate(), coupon.getStartDate());
		checkAmount(coupon.getAmount());
		checkType(coupon.getType());
		checkPrice(coupon.getPrice());
		checkImage(coupon.getImage());
		checkMessage(coupon.getMessage());		
	}

	private void checkTitle(String title) throws CouponSystemException {
		if(title.length()>COUPON_TITLE_LENGTH)
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_TITLE,"Coupon's title can't be longer than " + COUPON_TITLE_LENGTH + " characters");
	}

	private void checkStartDate(Date startDate) throws CouponSystemException {
		if(startDate.before(new java.util.Date(System.currentTimeMillis())))
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_START_DATE,"Coupon's start date can't be earlier than today");
	}

	private void checkEndDate(Date endDate, Date startDate) throws CouponSystemException {
		if(endDate.before(startDate))
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_END_DATE,"Coupon's expiration date can't be earlier than today");
	}

	private void checkAmount(int amount) throws CouponSystemException {
		if(amount<0)
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_AMOUNT,"Coupon's amount can't be negative");
	}

	private void checkType(CouponType type) throws CouponSystemException {
		if(type == null)
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_TYPE,"Coupon type is required");
	}

	private void checkPrice(double price) throws CouponSystemException {
		if(price<0)
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_PRICE,"Coupon's price can't be smaller than 0");
	}

	private void checkImage(String image) throws CouponSystemException {
		//TODO if not null check if file exists/valid
		if(image == null)
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_IMAGE,"Coupon image is required");
	}

	private void checkMessage(String message) throws CouponSystemException {
		if(message.length()>COUPON_MSG_LENGTH)
			throw new CouponSystemException(ExceptionsEnum.VALIDATION_COUPON_MESSAGE,"Coupon's message can't be longer than " + COUPON_MSG_LENGTH + " characters");		
	}
	
	private boolean customerAlreadyOwnsCoupon(long couponId, long customerId) throws CouponSystemException {
		CouponEntity coupon = this.getCoupon(couponId);//.getCustomers().contains(o)
		CustomerEntity customer = customerService.getCustomer(customerId);
		if(customer.getCoupons().contains(coupon)) {
			return true;
		}
		return false;
	}
	
	
}
