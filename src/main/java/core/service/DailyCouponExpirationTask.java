package core.service;

import java.io.Serializable;
import java.sql.Date;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import core.repository.ICouponRepository;

/**
 * A daily task that checks and deletes all the coupons in accordance to their expiration date.
 * 
 *
 */
@Service
public class DailyCouponExpirationTask extends Thread implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private ICouponRepository couponDAO;
	private long timeTillMidnight = 60000;
	private boolean quit = false; 

	@PostConstruct
	private void init() {
		this.start();
	}

	@Override
	public void run() {
		System.out.println("LOG : Daily Task started");
		//while coupon system still running
		while(!quit) {
			//Sleeps till midnight local time, on startup after set amount of millisecond 
			try {
				Thread.sleep(timeTillMidnight);
			} catch (InterruptedException e) {
				// TODO Manager handling
				// shld be picked by tomcat logger
				System.err.println("LOG : Daily task sleep interrupted");
//				e.printStackTrace();
				continue;
			}
						
			System.out.println("The Date is " + new Date(System.currentTimeMillis()));
				int dml = couponDAO.removeExpiredCoupons(new Date(System.currentTimeMillis()));
				System.out.println("LOG : Expired coupons removed : " + dml);
				
			//Sets the next iteration for 00:00 server local time
			timeTillMidnight = (long)((24*60*60*1000) - (System.currentTimeMillis() + TimeZone.getDefault().getRawOffset())%(24*60*60*1000));
		}
		System.out.println("LOG : Daily Task ended");
	}

	/**
	 * Stops the task.
	 */
	@PreDestroy
	public void stopTask() {
		System.out.println("LOG : Daily Task stopped");
		quit = true;
		interrupt();
	}

}
