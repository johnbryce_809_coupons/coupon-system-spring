package core.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import core.beans.CompanyBean;
import core.entities.CompanyEntity;
import core.enums.UserType;
import core.exception.CouponSystemException;
import core.exception.ExceptionsEnum;
import core.repository.ICompanyRepository;

/**
 * Facade used to access the coupon system by Companies
 *
 */
@Service
public class CompanyService implements Serializable, IBeanValidatorConstants{
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private ICompanyRepository companyDAO;

	/**
	 * Adds a new company entity to the repository.
	 * 
	 * @param company the new company entity to be added.
	 * @return the created company's ID. 
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as :
	 * 	existing name, (3) Invalid data.
	 */
	public long createCompany(CompanyBean company) throws CouponSystemException {
		//can be used if company registration should be restricted 
		checkCompany(company);
		//CLD BE HANDLED BY DAO LAYER BY MAKING IT UNIQUE
		if(companyDAO.existsByCompName(company.getCompName())) {
			throw new CouponSystemException(ExceptionsEnum.NAME_EXISTS,"Company Name already exists");
		}	
		long companyId = companyDAO.save(new CompanyEntity(company)).getId();
		System.out.println("LOG : Company created : " + company);
		return companyId;
	}


	/**
	 * Updates a company entity in the repository.
	 * 
	 * @param company the company object to be updated.
	 * @param userId the user updating the company
	 * @param userType the user type updating the company
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 * 	(3) Invalid data, (4) security breach.
	 */
	public void updateCompany(CompanyBean company, long userId, UserType userType) throws CouponSystemException {
		if ((company.getId() != userId || !userType.equals(UserType.COMPANY)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to update company %s",userType , userId, company));
		}
		
		CompanyEntity originalCompany = this.getCompany(company.getId());
		originalCompany.setEmail(company.getEmail());
		originalCompany.setCompName(company.getCompName());
		checkCompany(originalCompany.toBean());
		if(companyDAO.updateCompany(originalCompany.getCompName(), originalCompany.getEmail(), originalCompany.getId())==0) {
			//SHLD NEVER HAPPEN - CLIENT SIDE ERROR
			throw new CouponSystemException(ExceptionsEnum.FAILED_OPERATION,"update company failed, ID : " + company.getId());
		}
		System.out.println(String.format("LOG : User %s %s updated company %s",userType , userId, company));		
	}
	
	/**
	 * updates the company's password
	 * 
	 * @param companyId The company to update
	 * @param newPassword The new password
	 * @param oldPassword The old password
	 * @param userId the user updating the coupon
	 * @param userType the user type updating the coupon
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts.
	 */
	public void updateCompanyPassword(long companyId, String oldPassword, String newPassword, long userId, UserType userType) throws CouponSystemException {
		if ((companyId != userId || !userType.equals(UserType.COMPANY)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to change company's %s password ",userType , userId, companyId));
		}
		if(userType.equals(UserType.COMPANY)) {
			this.companyLogin(companyId, oldPassword);
		}
		this.checkCompanyPassword(newPassword);	
		if(companyDAO.updateCompanyPassword(companyId, newPassword)==0) {
			//SHLD NEVER HAPPEN - CLIENT SIDE ERROR
			throw new CouponSystemException(ExceptionsEnum.FAILED_OPERATION,"update company's password failed, ID : " + companyId);
		}
		System.out.println(String.format("LOG : User %s %s changed company's %s password ",userType , userId, companyId));
	}
	
	/**
	 * Removes a company entity from the companies repositories.
	 * removes the company's coupons as well.
	 * 
	 * @param companyId the company's ID.
	 * @param userId the user removing the company.
	 * @param userType the user type
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 *  (3) Invalid data, (4) security breach.
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void removeCompany(long companyId, long userId, UserType userType) throws CouponSystemException {
		//can be modified if company removing should be restricted
		if ((companyId != userId || !userType.equals(UserType.COMPANY)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to remove company %s",userType , userId, companyId));
		}

		removeCompanyCoupons(companyId);
		companyDAO.deleteById(companyId);
		System.out.println(String.format("LOG : User %s %s attempts to remove company %s",userType , userId, companyId));			
	}
	
	public void removeCompanyCoupons(long companyId) throws CouponSystemException {		
		CompanyEntity company = this.getCompany(companyId);
		System.out.println("Before"+company.getCoupons());
		company.remove();
		System.out.println("After"+company.getCoupons());
		companyDAO.save(company);//TODO is necessary ?
	}
	
	/**
	 * Retrieves a company entity from the repository.
	 * 
	 * @param companyId the company's ID.
	 * @return a CompanyBean object
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	public CompanyEntity getCompany(long companyId) throws CouponSystemException {	
		Optional<CompanyEntity> optionalCompany = companyDAO.findById(companyId);
		if(!optionalCompany.isPresent()) {
			throw new CouponSystemException(ExceptionsEnum.FAILED_OPERATION,"can't find company with id num : " + companyId);
		}
		
		return optionalCompany.get();
	}
	
	/**
	 * Retrieves all the companies entities from the repository .
	 * 
	 * @return a Collection of companies objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	public Collection<CompanyBean> getAllCompanies() throws CouponSystemException{
		Collection<CompanyBean> beans = new ArrayList<>();
		for (CompanyEntity companyEntity : companyDAO.findAll()) {
			beans.add(companyEntity.toBean());
		}		
		return beans;
	}

	/**
	 * Check the password and user name,
	 * returns the user ID if correct.
	 * 
	 * @param companyName The company's user name
	 * @param password The company's password
	 * @return a long userId - the user's ID
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 * 
	 */
	public long companyLogin(String name, String password) throws CouponSystemException {
		Optional<Long> optionalId = companyDAO.companyLogin(name, password);
		if(!optionalId.isPresent()) {
			throw new CouponSystemException(ExceptionsEnum.AUTHENTICATION,"Incorrect Name Or Password");
		}
		return optionalId.get();
	}
	public long companyLogin(long compId, String password) throws CouponSystemException {
		Optional<Long> optionalId = companyDAO.companyLogin(compId, password);
		if(!optionalId.isPresent()) {
			throw new CouponSystemException(ExceptionsEnum.AUTHENTICATION,"Incorrect ID Or Password");
		}
		return optionalId.get();
	}

	private void checkCompany(CompanyBean company) throws CouponSystemException {
		checkCompanyName(company.getCompName());
		checkCompanyPassword(company.getPassword());
		checkCompanyEmail(company.getEmail());
	}

	private void checkCompanyName(String compName) throws CouponSystemException {
		if (compName.length() > COMPANY_NAME_LENGTH) {
			CouponSystemException e = new CouponSystemException(ExceptionsEnum.VALIDATION_NAME,
					"The company name cant be more than " + COMPANY_NAME_LENGTH + " characters");
			throw e;
		}
	}

	private void checkCompanyPassword(String compPassword) throws CouponSystemException {
		if (compPassword.length() > PASSWORD_MAX_LENGTH) {
			CouponSystemException e = new CouponSystemException(ExceptionsEnum.VALIDATION_PASSWORD_MAX,
					"The company password cant be more than " + PASSWORD_MAX_LENGTH + " characters");
			throw e;
		}
		if (compPassword.length() < PASSWORD_MIN_LENGTH) {
			CouponSystemException e = new CouponSystemException(ExceptionsEnum.VALIDATION_PASSWORD_MIN,
					"The company password need to be more than " + PASSWORD_MIN_LENGTH + " characters");
			throw e;
		}
	}

	private void checkCompanyEmail(String companyEmail) throws CouponSystemException {
		if (companyEmail == null) {
			CouponSystemException e = new CouponSystemException(ExceptionsEnum.VALIDATION_EMAIL,
					"The company email cant be null");
			throw e;
		}

		if (companyEmail.length() > EMAIL_LENGTH){
			CouponSystemException e = new CouponSystemException(ExceptionsEnum.VALIDATION_EMAIL,
					"The company email cant be more than" + EMAIL_LENGTH + "leters");
			throw e;
		}

		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";
		Pattern pat = Pattern.compile(emailRegex);
		if (!pat.matcher(companyEmail).matches()) {
			CouponSystemException e = new CouponSystemException(ExceptionsEnum.VALIDATION_EMAIL,
					"The company email is not valid");
			throw e;
		}
	}

	public void save(CompanyEntity companyEntity) {
		companyDAO.save(companyEntity);
	}
}
