package core.service;

import java.io.Serializable;

/**
 * Collection of constants for validity checks
 * 
 */
public interface IBeanValidatorConstants extends Serializable{
	public static final int EMAIL_LENGTH = 50;
	public static final int PASSWORD_MAX_LENGTH = 10;
	public static final int PASSWORD_MIN_LENGTH = 6;
	public static final int COMPANY_NAME_LENGTH = 50;
	public static final int CUSTOMER_NAME_LENGTH = 50;
	public static final int COUPON_TITLE_LENGTH = 50;
	public static final int COUPON_MSG_LENGTH = 250;
	
}
