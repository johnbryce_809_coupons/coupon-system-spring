package core.test;

import core.exception.CouponSystemException;
import core.service.CompanyService;
import core.service.CouponService;
import core.service.CustomerService;

public abstract class GenericThread extends Thread{
	public CouponService couponService = new CouponService();
	public CompanyService companyService = new CompanyService();
	public CustomerService customerService = new CustomerService();
	
	protected void loginAdmin()  {
		System.out.println(Thread.currentThread().getName() + " : LOG : Admin logged in");
	}
	
	protected long loginCompany(String user, String password) throws CouponSystemException {
		System.out.println(Thread.currentThread().getName() + " : LOG : Company logged in : " + user);
		return companyService.companyLogin(user, password);
	}

	protected long loginCustomer(String user, String password) throws CouponSystemException {
		System.out.println(Thread.currentThread().getName() + " : LOG : customer logged in : " + user);
		return customerService.customerLogin(user, password);
	}

}
