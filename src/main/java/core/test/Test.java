package core.test;

import org.springframework.stereotype.Component;

/**
 * Tests the implementation of the coupon system and it's classes.
 *
 */
@Component
public class Test {
	
	public static void main(String[] args) {

		GenericThread testThread1 = new CreateThread();	
		GenericThread testThread2 = new ExceptionThread();	
		GenericThread testThread3 = new RestCreateThread();	
		GenericThread testThread4 = new PurchaseThread(0, 0);	
		GenericThread testThread5 = new PurchaseThread(0, 5);	
		GenericThread testThread6 = new PurchaseThread(0, 10);	
//		testThread1.start();	
//		testThread2.start();	
		testThread3.start();	
//		testThread4.start();	
//		testThread5.start();
//		testThread6.start();	
		try {
			testThread1.join();
			testThread2.join();
			testThread3.join();
			testThread4.join();
			testThread5.join();
			testThread6.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	}	
}
