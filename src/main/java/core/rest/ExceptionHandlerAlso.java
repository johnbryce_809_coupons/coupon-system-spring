package core.rest;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
//import javax.ws.rs.core.Context;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.ext.ExceptionMapper;
//import javax.ws.rs.ext.Provider;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import core.beans.ExceptionBean;
import core.exception.CouponSystemException;
import core.exception.ExceptionsEnum;


/**
 * Exception handler for all the Coupon System application exceptions
 *
 */
@ControllerAdvice
public class ExceptionHandlerAlso extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionBean> toResponse(Throwable exception, HttpServletRequest request) {
		ExceptionBean exceptionBean;
		ExceptionsEnum exceptionEnum;
		String externalMessage;
		String internalMessage;
		int statusCode;

		//all the known exceptions hendeld by type
		if (exception instanceof CouponSystemException) {
			CouponSystemException theException = (CouponSystemException)exception;
			//get the data from the exception.
			exceptionEnum = theException.getExceptionsEnum();
			statusCode = exceptionEnum.getStatusCode();
			internalMessage = exceptionEnum.getInternalMessage();
			
			//OPERATION ERRORS
			// OPERATIONS THAT SHLDNT NORMALLY FAIL
			//dml equals 0
			if (statusCode >= 1600 && statusCode < 1700) {
			// TODO appropriate handling, log
			}
			//EXTERNAL/DB ERRORS
			// EXTERNAL FACTORS SHLDNT BE MORE THAN OCCASIONALY DEPENDING ON OTHERS' STABILLITY
			if (statusCode >= 1700 && statusCode < 1800) {
				// TODO appropriate handling, log 
			}
			// CLIENT SIDE ERRORS WITH HIGHER OCCURENCES 
			if (statusCode >= 1800 && statusCode < 1900) {
				
			}
				// SECURITY ERRORS HIGH CHANCES OF BREACH ATTEMPTS
				// HIDES THE TRUE CAUSE
			if (statusCode >= 1900 && statusCode < 2000) {
				System.err.println("LOG : " + exception.getMessage());
//				exception.pr
				exceptionEnum = ExceptionsEnum.DATA_BASE_ERROR;
				statusCode = exceptionEnum.getStatusCode();
				internalMessage = exceptionEnum.getInternalMessage();
			}
			// CRITICAL ERRORS NOTIFY IMMIDIATLY
			if (statusCode >= 2000) {
				// TODO send email to manager + log
				exception.printStackTrace();
			}
		}else {
			//handle the uncaught exceptions by sending general message to the clint and print to log
			//TODO Log uncaught exceptions
			System.err.println("Different exception than CouponSystemException: ");
			exception.printStackTrace();
			exceptionEnum = ExceptionsEnum.FAILED_OPERATION;
			statusCode = exceptionEnum.getStatusCode();
			internalMessage = exceptionEnum.getInternalMessage();
		}
		///set the message to user and for client developer
		//TODO change only for developing purposes!!!! 
		externalMessage = getTranslatedMessage(exceptionEnum);
//		externalMessage = exception.getMessage();
		exceptionBean = new ExceptionBean(statusCode, externalMessage, internalMessage);
		System.out.println(statusCode);
		return ResponseEntity.status(statusCode).body(exceptionBean);	
	}
	
	//set the message that the user will see to his language 
	private String getTranslatedMessage(ExceptionsEnum exceptionEnum) {
		ResourceBundle errorMessages;
		//get the user language 
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
	            .getRequest();
		Locale locale = request.getLocale();
		try {
			//get the right translated file for user language 
			errorMessages = ResourceBundle.getBundle("core.exception.errorMessages", locale);			
		} catch (Exception e1) {
			// TODO LOG IN TRANSLATION LOG
			//if the requested language isn't supported, sets default to English 
			System.err.println(String.format("%s does not have error language support", locale.getDisplayLanguage()));
			locale = new Locale("en");
			try {
				errorMessages = ResourceBundle.getBundle("/CouponSystemWeb-App2-spring/src/main/java/core.exception.errorMessages", locale);		
			}catch (Exception e) {
				// TODO critical cldn't load errorMessages files
				System.err.println(String.format("Error message files not found "));
				return "error message unavailable";
			}
		}
		try {
			//return the translate message
			return errorMessages.getString(exceptionEnum.name());
		} catch (Exception e1) {
			// TODO LOG IN TRANSLATION LOG
			//if a translated message dosent  exist return as difolt the english one
			System.err.println(String.format("Error code %s does not have support for %s", String.valueOf(exceptionEnum.getStatusCode()), locale.getDisplayLanguage()));
			errorMessages = ResourceBundle.getBundle("core.exception.errorMessages_en");
			try {
				return errorMessages.getString(exceptionEnum.name());
			} catch (Exception e2) {
				// TODO LOG IN TRANSLATION LOG
				System.err.println(String.format("Error code %s does not have a default error message", String.valueOf(exceptionEnum.getStatusCode())));
				return "error message unavailable";
			}
		}
	}
}
