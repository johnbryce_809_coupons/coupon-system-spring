package core.rest.controller;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import core.beans.CartBean;
import core.beans.CouponBean;
import core.enums.CartStatus;
import core.enums.CouponType;
import core.enums.UserType;
import core.exception.CouponSystemException;
import core.exception.ExceptionsEnum;
import core.service.CouponService;

/**
 * An interface for a DAO class which provides access to {@link CouponBean} DTO
 * data type.
 * 
 *
 */
@RestController
@RequestMapping("/coupons")
public class CouponRestController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private CouponService couponService;


	private static final String UPLOAD_FOLDER = "../webapps/resources/coupons/";
//	private static final String UPLOAD_FOLDER = "resources/coupons";
//	private static final String UPLOAD_FOLDER = "C:\\resouces\\coupons";

	/**
	 * @return the created coupon's ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : existing title,
	 *                               (3) Invalid data, (4) security breach.
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public long createCoupon(@RequestBody CouponBean couponBean,  HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		checkIfBeanDontHaveNullData(couponBean);
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		return couponService.createCoupon(couponBean, userId, userType);
	}

	/**
	 * Adds a coupon to a customer entity, and updates the entity's amount in the
	 * repository. cannot be resolve if it results in a negative coupon's amount, or
	 * if customer already owns this coupon.
	 * 
	 * @param couponId the coupon's ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : out of stock,
	 *                               existing ownership or no matching data.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/{couponId}/{customerId}")
	public void purchaseCoupon(@PathVariable("couponId") long couponId, @PathVariable("customerId") long customerId,
			 HttpServletRequest httpServletRequest) throws CouponSystemException {
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		couponService.purchaseCoupon(couponId, customerId, userId, userType);
	}

	/**
	 * Adds a coupon to a customer entity, and updates the entity's amount in the
	 * repository. cannot be resolve if it results in a negative coupon's amount, or
	 * if customer already owns this coupon.
	 * 
	 * @param couponId the coupon's ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : out of stock,
	 *                               existing ownership or no matching data.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/cart/{customerId}")
	public CartBean checkoutCart(@RequestBody CartBean cartBean, @PathVariable("customerId") long customerId,
			 HttpServletRequest httpServletRequest) throws CouponSystemException {
		if(cartBean.getCoupons() == null || cartBean.getStatus()==null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL, "status or collection cant be null");
		}
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		return this.checkoutCart(cartBean, customerId, userId, userType);
	}

	/**
	 * Updates a coupon entity in the repository.
	 * 
	 * @param couponBean the CouponBean object to be updated.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data,
	 *                               (3) Invalid data, (4) security breach.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateCoupon(@RequestBody CouponBean couponBean,  HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		checkIfBeanDontHaveNullData(couponBean);
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		couponService.updateCoupon(couponBean, userId, userType);
//		couponService.updateCouponAmount(coupon, userId, userType);
	}

	/**
	 * Updates a coupon entity's amount in the repository. cannot be resolve if it
	 * results in a negative amount.
	 * 
	 * @param couponId    the coupon's ID.
	 * @param amountDelta the amount of coupons to be added or removed (negative
	 *                    amount).
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : negative delta to
	 *                               exceeds stock, no matching data, (3) Invalid
	 *                               data, (4) security breach.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/amount/{couponId}")
	public void updateCouponAmount(@PathVariable("couponId") long couponId, @RequestParam("amount") int amoutDelta,
			 HttpServletRequest httpServletRequest) throws CouponSystemException {
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		couponService.updateCouponAmout(couponId, amoutDelta, userId, userType);
	}

	/**
	 * Removes a coupon entity from the coupons and customers' coupons repositories.
	 * 
	 * @param couponId the coupon's ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data,
	 *                               (3) Invalid data, (4) security breach.
	 */
//	@DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@RequestMapping("/{couponId}")
	@DeleteMapping("/{couponId}")
	public void removeCoupon(@PathVariable("couponId") long couponId,  HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		couponService.removeCoupon(couponId, userId, userType);
	}

	/**
	 * Retrieves a coupon entity from the repository.
	 * 
	 * @param couponId the coupon's ID.
	 * @return a CouponBean object
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/{couponId}")
	public CouponBean getCoupon(@PathVariable("couponId") long couponID) throws CouponSystemException {
		return couponService.getCoupon(couponID).toBean();
	}

	/**
	 * Retrieves all the coupons entities of said type from the repository .
	 * 
	 * @param type the coupons Type.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/couponType")
	public Collection<CouponBean> getCouponsByType(@RequestParam("couponType") CouponType couponType)
			throws CouponSystemException {
		return couponService.getCouponsByType(couponType);
	}

	/**
	 * Retrieves all the coupons entities from the repository .
	 * 
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<CouponBean> getAllCoupons( HttpServletRequest request) throws CouponSystemException {
		return couponService.getAllCoupons();
	}

	/**
	 * Retrieves all the coupons entities for said Company from the repository .
	 * 
	 * @param companyId the company's Id.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/company/{companyId}")
	public Collection<CouponBean> getCompanyCoupons(@PathVariable("companyId") long companyId)
			throws CouponSystemException {
		return couponService.getCompanyCoupons(companyId);
	}

	/**
	 * Retrieves all the coupons entities of said Type for said Company from the
	 * repository .
	 * 
	 * @param companyId  the company's Id.
	 * @param couponType the coupons Type.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/company/{companyId}/couponType")
	public Collection<CouponBean> getCompanyCouponsByType(@RequestParam("couponType") CouponType couponType,
			@PathVariable("companyId") long companyId) throws CouponSystemException {
		return couponService.getCompanyCouponsByType(companyId, couponType);
	}

	/**
	 * Retrieves all the coupons entities bellow said Price for said Company from
	 * the repository .
	 * 
	 * @param companyId   the company's Id.
	 * @param couponPrice the coupons Price.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/company/{companyId}/couponPrice")
	public Collection<CouponBean> getCompanyCouponsByPrice(@PathVariable("companyId") long companyId,
			@RequestParam("couponPrice") double couponPrice) throws CouponSystemException {
		return couponService.getCompanyCouponsByPrice(companyId, couponPrice);
	}

	/**
	 * Retrieves all the coupons entities expiring before said Date for said Company
	 * from the repository .
	 * 
	 * @param companyId      the company's Id.
	 * @param expirationDate the latest (max) expiration Date.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/company/{companyId}/expirationDate")
	public Collection<CouponBean> getCompanyCouponsByDate(@PathVariable("companyId") long companyId,
			@RequestParam("expirationDate") Date expirationDate) throws CouponSystemException {
		return couponService.getCompanyCouponsByDate(companyId, expirationDate);
	}

	/**
	 * Retrieves all the coupons entities for said Customer from the repository .
	 * 
	 * @param customerId the customer's Id.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/customer/{customerId}")
	public Collection<CouponBean> getCustomerCoupons(@PathVariable("customerId") long customerId,
			 HttpServletRequest httpServletRequest) throws CouponSystemException {
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		return couponService.getCustomerCoupons(customerId, userId, userType);
	}

	/**
	 * Retrieves all the coupons entities of said Type for said Customer from the
	 * repository .
	 * 
	 * @param customerId the customer's Id.
	 * @param couponType the coupons Type.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/customer/{customerId}/couponType")
	public Collection<CouponBean> getCustomerCouponsByType(@PathVariable("customerId") long customerId,
			@RequestParam("couponType") CouponType couponType,  HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		return couponService.getCustomerCouponsByType(customerId, couponType, userId, userType);
	}

	/**
	 * Retrieves all the coupons entities bellow said Price for said Customer from
	 * the repository .
	 * 
	 * @param customerId  the customer's Id.
	 * @param couponPrice the coupons Price.
	 * @return a Collection of CouponBean objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/customer/{customerId}/couponPrice")
	public Collection<CouponBean> getCustomerCouponsByPrice(@PathVariable("customerId") long customerId,
			@RequestParam("couponPrice") double couponPrice,  HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		return couponService.getCustomerCouponsByPrice(customerId, couponPrice, userId, userType);
	}

	/**
	 * Returns text response to caller containing uploaded file location
	 * 
	 * @return error response in case of missing parameters an internal exception or
	 *         success response if file has been stored successfully
	 */
	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@RequestMapping("/uploadimage")
	public void uploadImage(@RequestParam("pic") MultipartFile file)
			throws CouponSystemException {

		// check if all form parameters are provided
		if (file == null)
//			return Response.status(400).entity("Invalid form data").build();
			throw new CouponSystemException(ExceptionsEnum.UPLOAD_FAILED, "Invalid Form Data");
		// create our destination folder, if it not exists
		try {
			createFolderIfNotExists(UPLOAD_FOLDER);
		} catch (SecurityException se) {
			throw new CouponSystemException(ExceptionsEnum.UPLOAD_FAILED, "Security Exception", se);
		}

		try {
//			new Path().;
			file.transferTo(Paths.get(UPLOAD_FOLDER+file.getOriginalFilename()));
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("file saved");

	}

	/**
	 * Creates a folder to desired location if it not already exists
	 * 
	 * @param dirName - full path to the folder
	 * @throws SecurityException - in case you don't have permission to create the
	 *                           folder
	 */
	private void createFolderIfNotExists(String dirName) throws SecurityException {
		File theDir = new File(dirName);
		if (!theDir.exists()) {
			if(theDir.mkdir()) {
				System.out.println("LOG : Directory created " + theDir.getAbsolutePath());
			}else {
				System.err.println("LOG : Failed to create dir " + theDir.getAbsolutePath());
			}
		}else if(!theDir.isDirectory()){
			System.err.println("LOG : Path is not a directory " + theDir.getAbsolutePath());
		}
	}

	private void checkIfBeanDontHaveNullData(CouponBean couponBean) throws CouponSystemException {
		if (couponBean.getTitle() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"title cant be null");
		}
		if (couponBean.getStartDate() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"start date cant be null");
		}
		if (couponBean.getEndDate() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"end date cant be null");
		}
//		if (couponBean.getAmount() == null) {
//			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL, "amount cant be null");
//		}
		if (couponBean.getType() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"type cant be null");
		}
		if (couponBean.getMessage() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL, "messagw cant be null");
		}
//		if (couponBean.getPrice() == null) {
//			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL, "price cant be null");
//		}
		if (couponBean.getImage() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL, "image cant be null");
		}
//		if (couponBean.getCompanyId() == null) {
//			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"companyId cant be null");
//		}
		
	}
	
	/**
	 * Adds a coupon to a customer entity, and updates the entity's amount in the repository.
	 * cannot be resolve if it results in a negative coupon's amount, or if customer already owns this coupon. 
	 * 
	 * @param couponId the coupon's ID.
	 * @param userId the user ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : out of stock,
	 *  existing ownership or no matching data.
	 */

	private CartBean checkoutCart(CartBean cartBean, long customerId, long userId, UserType userType) throws CouponSystemException {
		//TODO check if list doesn't contain a coupon twice, handled down the line and rare but cld save O
		Collection<Long> availableCoupons = new ArrayList<>();
		Collection<Long> refusedCoupons = new ArrayList<>();
		//checks userType
		if( (!userType.equals(UserType.CUSTOMER) || customerId != userId) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to purchase coupons %s on user %s", userType, userId, cartBean.getCoupons(), customerId));
		}
		
		for (Long couponId : cartBean.getCoupons()) {
			try {
				couponService.purchaseCoupon(couponId, customerId, userId, userType);
				availableCoupons.add(couponId);
			}catch (CouponSystemException e) {
				refusedCoupons.add(couponId);
			}	
		}
		if(refusedCoupons.size()==0/*&&(!paymentGateway.checkout(amount, account))*/) {
			//Begin payment transaction or Finish payment transaction depending on business logic
			
			System.out.println(String.format("User %s %s purchased coupon %s", userType, userId, cartBean.getCoupons()));
			cartBean.setStatus(CartStatus.PURCHASED);
			
		}else {
			for (Long couponId : availableCoupons) {
				try {
					couponService.cancelPurchaseCoupon(couponId, customerId, userId, userType);
				}catch (CouponSystemException e) {
					// TODO: Log failure to cancel purchase			
				}
			}
			if(refusedCoupons.size()==0) {
				cartBean.setStatus(CartStatus.DENIED);						
			}else {//payment was denied
				cartBean.setStatus(CartStatus.UPDATED);
			}
		}
		
		cartBean.setCoupons(availableCoupons);
		return cartBean;
	}
}
