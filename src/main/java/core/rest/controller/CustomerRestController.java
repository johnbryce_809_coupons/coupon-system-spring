package core.rest.controller;

import java.io.Serializable;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import core.beans.CustomerBean;
import core.beans.UserBean;
import core.enums.UserType;
import core.exception.CouponSystemException;
import core.exception.ExceptionsEnum;
import core.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerRestController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private CustomerService customerService;
	
	/**
	 * Adds a new customer entity to the repository.
	 * 
	 * @param customerBean the new customer entity to be added.
	 * @return the created customer's ID. 
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as :
	 * 	existing name, (3) Invalid data.
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public long createCustomer(@RequestBody CustomerBean customerBean, HttpServletRequest httpServletRequest) throws CouponSystemException {
		checkIfBeanDontHaveNullData(customerBean);
		return customerService.createCustomer(customerBean);			
	}
	
	/**
	 * Updates a customer entity in the repository.
	 * 
	 * @param customerBean the customer object to be updated.
	 * @param userId the user updating the customer
	 * @param userType the user type updating the customer
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 * 	(3) Invalid data, (4) security breach.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateCustomer(@RequestBody CustomerBean customerBean,  HttpServletRequest httpServletRequest) throws CouponSystemException {		
		checkIfBeanDontHaveNullData(customerBean);
		long userId = ((Long)httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType)httpServletRequest.getAttribute("userType"));
		customerService.updateCustomer(customerBean, userId, userType);
	}
	
	/**
	 * updates the customer's password
	 * 
	 * @param customerId The customer to update
	 * @param newPassword The new password
	 * @param oldPassword The old password
	 * @param userId the user updating the coupon
	 * @param userType the user type updating the coupon
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/{customerId}/password")
	public void updateCustomerPassword(@RequestBody UserBean userBean, @PathVariable("customerId") long customerId,  HttpServletRequest httpServletRequest) throws CouponSystemException {
		if(userBean.getUserName() ==null || userBean.getUserPassword() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"name or password cant be null");
		}
		long userId = ((Long)httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType)httpServletRequest.getAttribute("userType"));
		customerService.updateCustomerPassword(customerId, userBean.getUserName(), userBean.getUserPassword(), userId, userType);
	}
	
	/**
	 * Removes a customer entity from the customers repositories.
	 * removes the customer's coupons as well.
	 * 
	 * @param customerId the customer's ID.
	 * @param userId the user removing the customer.
	 * @param userType the user type
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data,
	 *  (3) Invalid data, (4) security breach.
	 */

	@DeleteMapping("/{customerId}")
	public void removeCustomer(@PathVariable("customerId") long customerId,  HttpServletRequest httpServletRequest) throws CouponSystemException {			
		long userId = ((Long)httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType)httpServletRequest.getAttribute("userType"));
		customerService.removeCustomer(customerId, userId, userType);				
	}
	
	/**
	 * Retrieves a customer entity from the repository.
	 * 
	 * @param customerId the customer's ID.
	 * @return a CompanyBean object
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/{customerId}")
	public CustomerBean getCustomer(@PathVariable("customerId") long customerId,  HttpServletRequest httpServletRequest) throws CouponSystemException {
		long userId = ((Long)httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType)httpServletRequest.getAttribute("userType"));
		if ((customerId != userId || !userType.equals(UserType.CUSTOMER)) && !userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to view customer %s",userType , userId, customerId));
		}
		return customerService.getCustomer(customerId).toBean();
	}
	
	/**
	 * Retrieves all the customers entities from the repository .
	 * 
	 * @return a Collection of customers objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error, (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<CustomerBean> getAllCustomers(HttpServletRequest httpServletRequest) throws CouponSystemException{
		long userId = ((Long)httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType)httpServletRequest.getAttribute("userType"));
		if (!userType.equals(UserType.ADMIN)) {
			throw new CouponSystemException(ExceptionsEnum.SECURITY_BREACH,String.format("User %s %s attempts to view customers",userType , userId));
		}
		return customerService.getAllCustomers();
	}
	
	private void checkIfBeanDontHaveNullData(CustomerBean customerBean) throws CouponSystemException {
		if (customerBean.getCustName()== null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"name cant be null");
		}
		if (customerBean.getPassword() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"password cant be null");
		}
	}
}
