package core.rest.controller;

import java.io.Serializable;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import core.beans.CompanyBean;
import core.beans.UserBean;
import core.enums.UserType;
import core.exception.CouponSystemException;
import core.exception.ExceptionsEnum;
import core.service.CompanyService;

@RestController
@RequestMapping("/companies")
public class CompanyRestController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private CompanyService companyService;

	/**
	 * Adds a new company entity to the repository.
	 * 
	 * @param company the new company entity to be added.
	 * @return the created company's ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : existing name, (3)
	 *                               Invalid data.
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public long createCompany(@RequestBody CompanyBean companyBean, HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		checkIfBeanDontHaveNullData(companyBean);
		return companyService.createCompany(companyBean);
	}

	/**
	 * Updates a company entity in the repository.
	 * 
	 * @param companyBean the company object to be updated.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data,
	 *                               (3) Invalid data, (4) security breach.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void updateCompany(@RequestBody CompanyBean companyBean,  HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		checkIfBeanDontHaveNullData(companyBean);
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		companyService.updateCompany(companyBean, userId, userType);
	}

	/**
	 * updates the company's password
	 * 
	 * @param companyId   The company to update
	 * @param newPassword The new password
	 * @param oldPassword The old password
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts.
	 */
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/{companyId}/password")
	public void updateCompanyPassword(@RequestBody UserBean userBean, @PathVariable("companyId") long companyId,
			 HttpServletRequest httpServletRequest) throws CouponSystemException {
		if(userBean.getUserName() ==null || userBean.getUserPassword() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"name or password cant be null");
		}
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		// using userBean as substitute for loginBean, same on client side
		companyService.updateCompanyPassword(companyId, userBean.getUserName(), userBean.getUserPassword(),
				userId, userType);
	}

	/**
	 * Removes a company entity from the companies repositories. removes the
	 * company's coupons as well.
	 * 
	 * @param companyId the company's ID.
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data,
	 *                               (3) Invalid data, (4) security breach.
	 */
//	@DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@RequestMapping("/{companyId}")
	@DeleteMapping("/{companyId}")
	public void removeCompany(@PathVariable("companyId") long companyId,  HttpServletRequest httpServletRequest)
			throws CouponSystemException {
		long userId = ((Long) httpServletRequest.getAttribute("userId")).longValue();
		UserType userType = ((UserType) httpServletRequest.getAttribute("userType"));
		companyService.removeCompany(companyId, userId, userType);
	}

	/**
	 * Retrieves a company entity from the repository.
	 * 
	 * @param companyId the company's ID.
	 * @return a CompanyBean object
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/{companyId}")
	public CompanyBean getCompany(@PathVariable("companyId") long companyId) throws CouponSystemException {
		return companyService.getCompany(companyId).toBean();
	}

	/**
	 * Retrieves all the companies entities from the repository .
	 * 
	 * @return a Collection of companies objects
	 * @throws CouponSystemException if the operation failed due to (1) DB error,
	 *                               (2) data conflicts such as : no matching data.
	 */
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<CompanyBean> getAllCompanies() throws CouponSystemException {
		return companyService.getAllCompanies();
	}

	private void checkIfBeanDontHaveNullData(CompanyBean companyBean) throws CouponSystemException {
		if (companyBean.getCompName() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"name cant be null");
		}
		if (companyBean.getEmail() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"email cant be null");
		}
		if (companyBean.getPassword() == null) {
			throw new CouponSystemException(ExceptionsEnum.DATA_CANT_BE_NULL,"password cant be null");
		}
	}
	
}
