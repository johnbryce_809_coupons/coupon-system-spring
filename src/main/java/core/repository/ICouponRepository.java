/**
 * 
 */
package core.repository;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import core.beans.CouponBean;
import core.entities.CouponEntity;
import core.enums.CouponType;

/**
 * An interface for a DAO class which provides access to {@link CouponBean} DTO data type.
 * 
 *
 */
@Repository
public interface ICouponRepository extends Serializable,CrudRepository<CouponEntity, Long>{
	
	@Modifying
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Query("UPDATE CouponEntity c SET c.amount=c.amount+:amountDelta WHERE c.couponId=:couponId AND c.amount+:amountDelta>=0 ")
	int updateCouponAmount(@Param("couponId") long couponId,@Param("amountDelta") int amountDelta);
	
	@Modifying
	@Transactional
	@Query("DELETE FROM CouponEntity c WHERE c.endDate < :currentDate")
	int removeExpiredCoupons(@Param("currentDate") Date currentDate);
	
	Collection<CouponEntity> findByType(CouponType type);
	
	Collection<CouponEntity> getCouponsByCompany_id(long company_id);
	
	Collection<CouponEntity> getCouponsByCompany_idAndType(long company_id, CouponType type);
	
	Collection<CouponEntity> getCouponsByCompany_idAndPrice(long company_id, double price);

	Collection<CouponEntity> getCouponsByCompany_idAndEndDate(long company_id, Date endDate);
	
	Collection<CouponEntity> getCouponsByCustomers_id(long customers_id);
	
	Collection<CouponEntity> getCouponsByCustomers_idAndType(long customers_id, CouponType type);
	
	Collection<CouponEntity> getCouponsByCustomers_idAndPrice(long customers_id, double price);

	boolean existsByTitle(String title);
}
