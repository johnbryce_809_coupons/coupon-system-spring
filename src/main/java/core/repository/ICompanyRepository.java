package core.repository;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import core.beans.CompanyBean;
import core.entities.CompanyEntity;

/**
 * An interface for a DAO class which accesses a {@link CompanyBean} type DAO.
 *
 *
 */
@Repository
public interface ICompanyRepository extends Serializable, CrudRepository<CompanyEntity, Long>{

	@Modifying
	@Transactional
	@Query("UPDATE CompanyEntity c SET c.compName=:compName, c.email=:email  WHERE c.id=:companyId")
	int updateCompany(@Param("compName") String compName,@Param("email") String email,@Param("companyId") long companyId);

	@Modifying
	@Transactional
	@Query("UPDATE CompanyEntity c SET c.password=:newPassword WHERE c.id=:companyId ")
	int updateCompanyPassword(@Param("companyId") long companyId,@Param("newPassword") String newPassword);
	
	@Query("SELECT c.id FROM CompanyEntity c WHERE c.compName=:compName AND c.password=:password")
	Optional<Long> companyLogin(@Param("compName") String compName,@Param("password") String password);
	
	@Query("SELECT c.id FROM CompanyEntity c WHERE c.id=:compId AND c.password=:password")
	Optional<Long> companyLogin(@Param("compId") long compId,@Param("password") String password);
	
	boolean existsByCompName(@Param("compName") String compName);
}
