package core.repository;


import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import core.beans.CustomerBean;
import core.entities.CustomerEntity;

/**
 * An interface for a DAO class which accesses a {@link CustomerBean} type DAO.
 *
 *
 */
@Repository
public interface ICustomerRepository extends Serializable, CrudRepository<CustomerEntity, Long>  {

	@Modifying
	@Transactional
	@Query("UPDATE CustomerEntity c SET c.custName=:custName WHERE c.id=:customerId ")
	int updateCustomer(@Param("custName") String custName,@Param("customerId") long customerId);

	@Modifying	
	@Transactional
	@Query("UPDATE CustomerEntity c SET c.password=:newPassword WHERE c.id=:customerId ")
	int updateCustomerPassword(@Param("customerId") long customerId,@Param("newPassword") String newPassword);
	
	@Query("SELECT c.id FROM CustomerEntity c WHERE c.custName=:custName AND c.password=:password")
	Optional<Long> customerLogin(@Param("custName") String custName,@Param("password") String password);
	
	@Query("SELECT c.id FROM CustomerEntity c WHERE c.id=:custId AND c.password=:password")
	Optional<Long> customerLogin(@Param("custId") long custId,@Param("password") String password);	
	
	boolean existsByCustName(String custName);
}

