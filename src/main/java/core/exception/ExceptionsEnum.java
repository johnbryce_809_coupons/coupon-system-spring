package core.exception;
/**
 * Enum for deferment type of exceptions
 */
public enum ExceptionsEnum {
	
	//THE OPERATION DID NOT RESULT IN ANY CHANGE - POSSIBLE SECURITY BREACH 
	
	/**************************       COMMON ERRORS         **********************************/
	/********   OPERATION ERRORS  **********************************/
	SQL_DML_ZERO(1601,"No records where altered/found + more technical information"),
	COUPON_NOT_CREATED(1602,"The Coupon could not be created + more technical information"),
	COUPON_NOT_PURCHASED(1603,"The Coupon could not be purchased + more technical information"),
	COUPON_NOT_UPDATED(1604,"The Coupon could not be updated + more technical information"),
	COUPON_NOT_REMOVED(1605,"The Coupon could not be removed + more technical information"),
	AMOUNT_NOT_UPDATED(1606,"The Stock could not be updated + more technical information"),
	GET_COUPON_FAILED(1607,"The Coupon could not be retrieved + more technical information"),

	/********   EXTERNAL/DB ERRORS  **********************************/
	
	DATA_CONFLICTS(1701,"The Data does not match records"),
	GENERATE_ID_NOT_RETRIEVED(1702,"Generated ID could not be retrieved"),
	DATA_BASE_ERROR(1704,"Data Base Error"),
	DATA_BASE_ACCSESS(1705,"Data Base Access"),
	DATA_BASE_TIMOUT(1706,"Data Base Timeout"),
	FAILED_OPERATION(1707,"General/Undefined Operational Error"),
	CONNECTION_POOL_FAILURE(1708,"Connection pool failure"),
	CONNECTION_POOL_CLOSING(1709,"Connection pool closing"),
	IO_EXCEPTION(1710,"IOException"),
	
	
	/********   CLIENT ERRORS  **********************************/
	
	AUTHENTICATION(1801,"Add Client Side Developer Message"),
	VALIDATION(1802,"Add Client Side Developer Message"),
	NAME_EXISTS(1803,"Add Client Side Developer Message"),
	BAD_NAME_OR_PASSWORD(1804,"Add Client Side Developer Message"),
	USER_TYPE_REQUIRED(1805,"Add Client Side Developer Message"),
	USER_NAME_REQUIRED(1806,"Add Client Side Developer Message"),
	USER_PASSWORD_REQUIRED(1807,"Add Client Side Developer Message"),
	NULL_DATA(1808,"Add Client Side Developer Message"),
	CUSTOMER_OWNS_COUPON(1809,"Add Client Side Developer Message"),
	ID_EXISTS(1810,"Add Client Side Developer Message"),
	NO_COOKIES(1811,"Add Client Side Developer Message"),
	REST_ERROR(1812,"Add Client Side Developer Message"),
	UPLOAD_FAILED(1813,"Add Client Side Developer Message"),
	DATA_CANT_BE_NULL(1814,"Add Client Side Developer Message"),
	VALIDATION_COUPON_TITLE(1815,"Coupon's title is too long"),
	VALIDATION_COUPON_START_DATE(1816,"Coupon's start date can't be earlier than today"),
	VALIDATION_COUPON_END_DATE(1817,"Coupon's expiration date can't be earlier than start date"),
	VALIDATION_COUPON_AMOUNT(1818,"Coupon's amount can't be negative"),
	VALIDATION_COUPON_TYPE(1819,"Coupon type is required"),
	VALIDATION_COUPON_PRICE(1820,"Coupon's price can't be smaller than 0"),
	VALIDATION_COUPON_IMAGE(1821,"Coupon image is required"),
	VALIDATION_COUPON_MESSAGE(1822,"Coupon's message is required"),
	VALIDATION_EMAIL(1823,"Email is not valid"),
	VALIDATION_PASSWORD_MIN(182,"Password is too short"),
	VALIDATION_PASSWORD_MAX(182,"password is too long"),
	VALIDATION_NAME(182,"Name is too long"),
	
	
	/**************************       SECURITY ERRORS         **********************************/

	UNAUTHORIZED(1901,"Add Client Side Developer Message"),
	SECURITY_BREACH(1902,"Add Client Side Developer Message"),
	
	
	
	/**************************       CRITICAL ERRORS         **********************************/
	IDGENERATOR_INIT_ERROR(2001,"Shouldn't go to Client Side Developer Message"),
	CONNECTION_POOL_INIT_ERROR(2002,"Shouldn't go to Client Side Developer Message"),
	TEST(2003,"Shouldn't go to Client Side Developer Message"), 
	CANCEL_PURCHASE_CUSTOMER_FAILED(2004,"Shouldn't go to Client Side Developer Message"),  
	ROLLBACK_FAILED(2005,"Rollback operation failed"),
	;
	
	
	
	
	
	
	private final int statusCode;
	private final String internalMessage;
	
	
	public String getInternalMessage() {
		return internalMessage;
	}


	private ExceptionsEnum(int statusCode, String internalMessage) {
		this.statusCode = statusCode;
		this.internalMessage = internalMessage;
	}


	public int getStatusCode() {
		return statusCode;
	}
	

}
