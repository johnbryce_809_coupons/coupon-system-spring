/**
 * 
 */
package core.enums;

import java.io.Serializable;

/**
 * Represents the available coupon types
 *
 */
public enum CouponType implements Serializable{
	RESTAURANTS,
	ELECTRICITY,
	FOOD,
	HEALTH,
	SPORTS,
	CAMPING,
	TRAVELLING;
}

