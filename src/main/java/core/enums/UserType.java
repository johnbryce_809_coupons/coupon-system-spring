package core.enums;

/**
 * An Enum representation of Client types
 *
 */
public enum UserType {
	ADMIN,
	COMPANY,
	CUSTOMER;
}
